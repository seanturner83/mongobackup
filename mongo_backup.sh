#!/bin/bash
#--
# CONFIG
DBS=$*
DUMP_LOCATION=/backup/mongodb/`hostname -s`
LOGS_LOCATION=/var/log/mongo_backup/`hostname -s`
DIR_NAME=db-`date +%Y%m%d%H`
LOG_FILE=$LOGS_LOCATION/log-`date +%Y%m%d%H`
DIR_NAME_ABS=$DUMP_LOCATION/$DIR_NAME
COPY_SSH_HOST=backup
COPY_SSH_DEST=/backup/mongodb/`hostname -s`
# Uncomment and configure
#USERNAME="username"
#PASSWORD="password"
##--
AUTH=""

if [ -n "$USERNAME" ]
        then    AUTH=" -u $USERNAME -p $PASSWORD";
fi

if [ -z "$DBS" ]
        then echo "Usage: mongo_backup.sh db1 db2 db3 ... dbn";
        exit;
fi

mkdir -p $LOGS_LOCATION
echo "Starting backup `date`" >> $LOG_FILE

for DB in $DBS
do
        echo "Backup: $DB" >> $LOG_FILE
        mkdir -p $DIR_NAME_ABS
        /usr/bin/mongodump --db $DB $AUTH --out $DIR_NAME_ABS >> $LOG_FILE
        mkdir -p $DUMP_LOCATION/$DB
        ssh $COPY_SSH_HOST 'mkdir -p $1' -- "$COPY_SSH_DEST/$DB"
#       echo "here"
        if [[ `ls $DUMP_LOCATION/$DB | wc -l` -gt 6 ]]
                then
                        echo "More than 6 backups found, deleting older than 2 days" >> $LOG_FILE;
                        find $DUMP_LOCATION/$DB -type f -mtime +1 -exec rm {} \;
        fi
        echo "Tarring and gzipping backed up DB: $DB" >> $LOG_FILE
        tar -czvf $DUMP_LOCATION/$DB/$DIR_NAME.tgz --remove-files --directory $DUMP_LOCATION $DIR_NAME >> $LOG_FILE
        echo "Copying $DIR_NAME.tgz to $COPY_SSH_HOST folder $COPY_SSH_DEST/$DB" >> $LOG_FILE
        scp     $DUMP_LOCATION/$DB/$DIR_NAME.tgz $COPY_SSH_HOST:$COPY_SSH_DEST/$DB/ >> $LOG_FILE
done
echo "Backup Finished" >> $LOG_FILE