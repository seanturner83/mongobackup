#!/bin/bash
#--
# CONFIG
DBS="StateAndAnalytics_withIp"
DUMP_LOCATION=/db/mongo_backup
LOGS_LOCATION=/var/log/mongo_backup
DIR_NAME=db-`date +%Y%m%d`
LOG_FILE=$LOGS_LOCATION/log-`date +%Y%m%d`
DIR_NAME_ABS=$DUMP_LOCATION/$DIR_NAME

echo "Starting restore `date`" >> $LOG_FILE

for DB in $DBS
do
        tar -xvzf $DUMP_LOCATION/$DB/$DIR_NAME.tgz --directory $DUMP_LOCATION $DIR_NAME >> $LOG_FILE
        echo "Remove: $DB" >> $LOG_FILE
        echo "db.dropDatabase()" | mongo $DB
        echo "Restore: $DB" >> $LOG_FILE
        mongorestore $DIR_NAME_ABS >> $LOG_FILE
        rm -rf $DIR_NAME_ABS
        if [[ `ls $DUMP_LOCATION/$DB | wc -l` > 4 ]]
                then
                        find $DUMP_LOCATION/$DB -type f -mtime +4 -exec rm {} \;
        fi
done
echo "Restore Finished" >> $LOG_FILE

